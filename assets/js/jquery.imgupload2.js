/**
 *	Author: Darril Louie Ramos
 *	ViiWorks Inc.
 *	Description: Transforms input tags into ajax-upload images.
 *	
 *	Sample Usage: $('#uniq-id').imgupload2();
 *
 */

;(function($) {

	var img_input;
	var methods = {
		init : function (options)
			{
				var defaults = {
					extra_data : {},
                    destroy_on_remove : false
				};
				
				var opts = $.extend({}, defaults, options);
			
				var img_input = this;
				var img_extra = this.data('img-type');
				var upload_fxn = this.data('upload-fxn');
				var id = this.attr('id');
				
				if(!id || id == ''){
					id = 'imgupload-' + Math.round(new Date("2013-09-05 15:34:00").getTime()/1000);
					$(img_input).attr('id', id);
				}
				
				var input_same_ids = $('input').filter(function(){ return $(this).attr('id') == id; });
				if(input_same_ids.length > 1){
					input_same_ids.each(function(index){
						var new_id_temp = id+'-'+index;
						if(this == img_input) id = new_id_temp;
						$(this).attr('id', new_id_temp);
					});
				}
				
				var image_holder = this.parent();
				var img_src = (this.data('img-empty')) ? this.data('img-empty') : '';
				var img_set = (this.data('img-src')) ? this.data('img-src') : '';
				var dashboard_url = this.data('dashboard-url');
				var upload_url = this.data('upload-url') ? this.data('upload-url') : '';
				var snapshot_url = this.data('snapshot-url');
				var upload_folder = this.data('upload-folder');
				var custom_file_name = this.data('file-name');
				var custom_file_name_input = '';
				var prev_img = '';
				var border_style = this.data('border');
				var border_hover = this.data('border-hover');
				var file = (img_src && img_src != '') ? img_src.split('/') : '';
				var file_path = this.data('file-path');
				var img_display_style = '';
				var upload_xhr;
				var snapshot_photo = null;
                
                var img_init_display = img_set != '' ? img_set : img_src;
                var img_rmv_display_style = img_set == '' ? 'display:none;' : '';
                
                if (opts.destroy_on_remove) img_rmv_display_style = '';
                
				$('#'+id).addClass('ajax_image_uploader');
				
				if(custom_file_name){
					custom_file_name_input = '<input type="hidden" name="file_name" value="'+custom_file_name+'" />';
				}
				
				// if(img_set == ''){img_display_style = 'display:none;'}
				
				this.css('display', 'none');
				image_holder.html(img_input);
				if( $('#'+id+'-upload-btn').length ==0 )
				image_holder.append(
					'<div style="display:inline-block;text-align:left;" class="image-holder">'+
					'<a class="btn btn-small btn-success" id="'+id+'-upload-btn" href="javascript:;" data-rel="tooltip" title="Upload Image" style="color:white;margin-left:3px;display:none;font-size: 21px;font-weight: bold;" >&plus;</a>'+
					// '<a class="btn btn-small btn-success" id="'+id+'-snapshot-btn" href="javascript:;" title="Take Photo" data-rel="tooltip" style="margin-left:3px;"><i class="icon-camera"></i></a>'+
					'<a class="btn btn-small btn-danger" id="'+id+'-remove-btn" href="javascript:;" title="Remove" style="'+img_rmv_display_style+'color:white;margin-left:3px;font-size: 21px;font-weight: bold;" >&times;</a>'+
					// '<div id="'+id+'-bar-loader-container" style="display:none;"></div>'+
					// '<div id="'+id+'-div-rel-container" style="width:100%;position:relative;">'+
					'<div id="'+id+'-div-rel-container" style="width:100%;position:relative;">'+
					'<div style="margin-top:5px;display:none;height:15px;width:'+image_holder.css('width')+';opacity:1;background-color:#ddd;box-shadow: inset 0 0 2px #000;-webkit-box-shadow: inset 0 0 2px #000;-moz-box-shadow: inset 0 0 2px #000;" class="progress_wrapper"><div class="upload_progress" style="box-shadow:rgb(79, 153, 198) 0px 0px 4px;-webkit-box-shadow:rgb(79, 153, 198) 0px 0px 4px;-moz-box-shadow:rgb(79, 153, 198) 0px 0px 4px;color:#fff;font-size:8px;background-color:#4f99c6;width:0%;text-align:right;line-height:15px;position:absolute">&nbsp;&nbsp;0%&nbsp;&nbsp;</div><div style="height:100%;width:100%;background-color:transparent;position:relative;"></div></div>'+
					// '<div style="display:none;position:absolute;width:100%;-webkit-filter:grayscale(100%);filter: grayscale(100%);-moz-filter: grayscale(100%);filter: grayscale(100%);"><img class="img" src="'+img_src+'" style="max-width:'+image_holder.css('width')+';'+img_display_style+'" /></div>'+
					// '<div style="position:absolute;height:100%;width:100%;"><img class="img" src="'+img_src+'" style="max-width:'+image_holder.css('width')+';'+img_display_style+'" /></div>'+
					'<div id="'+id+'-upload-image-container" style="'+img_display_style+'margin-top:5px;width:100%;overflow:hidden;position:relative;box-shadow: 0 0 5px #000;-webkit-box-shadow: 0 0 5px #000;-moz-box-shadow: 0 0 5px #000;"><img class="img colored" src="'+img_init_display+'" style="max-width:'+image_holder.css('width')+';'+img_display_style+'" /></div>'+
					'<div style="'+img_display_style+'margin-top:5px;width:100%;overflow:hidden;position:relative;box-shadow: 0 0 5px #000;-webkit-box-shadow: 0 0 5px #000;-moz-box-shadow: 0 0 5px #000;"><a href="javascript:;" id="'+id+'-capture-snapshot" class="btn btn-mini btn-success" style="position:absolute;"> Capture </a><div id="'+id+'-snapshot-container"></div></div>'+
					'</div>'+
					'</div>'
				);
				$('#'+id+'-upload-btn').show();
				if( $('#'+id+'-img-upload').length ==0 )
				this.closest('form').parent().append(
					// '<form style="display:none" id="'+id+'-img-upload" class="ajax-img-upload-form" action="'+dashboard_url+'imgupload/'+upload_fxn+'" target="'+id+'-iframe-post-form" method="post" enctype="multipart/form-data">'+
					'<form style="display:none" id="'+id+'-img-upload" class="ajax-img-upload-form" action="'+upload_url+'" target="'+id+'-iframe-post-form" method="post" enctype="multipart/form-data">'+
						'<input id="'+id+'-img-input" name="userfile" type="file" class="btn" style="display:none" accept="image/jpg, image/jpeg, image/png, image/bmp"/>'+
						'<input type="hidden" name="upload_path" value="'+upload_folder+'" />'+custom_file_name_input+
						'<input type="hidden" name="file" value="'+file[file.length-1]+'" />'+
					'</form>'+
					'<iframe id="'+id+'-iframe-post-form" name="'+id+'-iframe-post-form" style="display:none"></iframe>'
				);
				
				// this.val( image_holder.find('.img').attr("src") );
				
				
				//UPLOAD
				$('#'+id+'-upload-btn').unbind();
				$('#'+id+'-upload-btn').click(function() {
					$('#'+id+'-img-input').click();
					if(snapshot_photo !== null) snapshot_photo.stop();
					$('#'+id+'-snapshot-container').parent().hide();
					$('#'+id+'-upload-image-container').show();
				});
				
				//SNAPSHOT
				$('#'+id+'-snapshot-btn').unbind();
				$('#'+id+'-snapshot-btn').click(function() {
					if(snapshot_photo === null) 
						snapshot_photo = new SayCheese('#'+id+'-snapshot-container', {audio: false});
					
					$('#'+id+'-upload-image-container').hide();
					$('#'+id+'-snapshot-container').html('');
					$('#'+id+'-snapshot-container').parent().show();
					$('#'+id+'-snapshot-container').parent().css('display', 'inline-block');
					
					snapshot_photo.on("start", function(){
						$('#'+id+'-capture-snapshot').on("click", function(){
							snapshot_photo.takeSnapshot();
						});
						$('#'+id+'-snapshot-container video').css('width', '100%');
						$('#'+id+'-remove-btn').show();
					});
					
					snapshot_photo.on("stop", function(){
						$('#'+id+'-snapshot-container').empty();
					});
					
					snapshot_photo.on("snapshot", function(snapshot){
			
						var img = document.createElement("img");
						img.style.border = "1px solid #aaaaaa !Important;";
						img.style.width = "400";
						img.style.height = "400";
						
						img.src = snapshot.toDataURL("image/jpg");
						
						/*
							Sending image to AJAX for file creation
						*/
						var token = $('#'+id+'-snapshot-container video').attr('src').toString();
						token_arr = token.split('-');
						
						var extra_data_str = '';
						
						for(props in opts.extra_data){
							extra_data_str += '&' + props + '=' + opts.extra_data[props];
						}
						
						var params = 'tokenn=' + token_arr[token_arr.length-1] + '' + extra_data_str + '&dataURL=' + encodeURIComponent(img.src);
						
						var request = new XMLHttpRequest();
						request.open("POST", snapshot_url, true);
						request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
						request.onreadystatechange = function() {
							if (request.readyState == 4) {
								$('#'+id).val(request.responseText);
								$('#'+id+'-percentage-charts-loader').fadeOut(400/*, function(){ $(this).remove(); }*/);
							}
						}
						request.send(params);
						
						var ht = $('#'+id+'-snapshot-container video').css('height');
						ht = ht.replace("px", '') * 1;
						var margin = ht;
						ht = (ht * 0.15).toFixed();
						var margin = ((margin - ht) / 2).toFixed();
						
						snapshot_photo.stop();
						
						image_holder.find('.img.colored').attr('src', img.src);
						image_holder.find('.img.colored').show();
						$('#'+id+'-snapshot-container').parent().hide();
						var loading='<div id="'+id+'-percentage-charts-loader" style="display:none;z-index: 9;background: white;width: 100%;height: 100%;opacity: .75;position: absolute;text-align: center;">'+
							'<div style="display:block;vertical-align:middle;font-size: '+ht+'px;margin-top:'+margin+'px;">Uploading...</div></div>';
						$('#'+id+'-upload-image-container').prepend(loading);
						$('#'+id+'-upload-image-container').show();
						$('#'+id+'-percentage-charts-loader').fadeIn();
					});
					
					snapshot_photo.start();
				});
				
				$('#'+id+'-remove-btn').unbind();
				$('#'+id+'-remove-btn').click(function(){
					if(snapshot_photo !== null) snapshot_photo.stop();
					$('#'+id+'-snapshot-container').parent().hide();
					$('#'+id+'-upload-image-container').show();
					
					$(this).hide();
					if(img_src == '') image_holder.find('.img').hide();
					$('#'+id+'-img-input').val('');
					image_holder.find('.img.colored').attr('src', img_src);
					// image_holder.find('.img.colored').parent().css('width', '0%');
					$('#'+id).val('');
					if(upload_xhr !== undefined){
						upload_xhr.abort();
						image_holder.find('.upload_progress').stop();
						image_holder.find('.upload_progress').html('&nbsp;&nbsp;Cancelled...&nbsp;&nbsp;');
					}
                    
                    if (opts.destroy_on_remove) $('#'+id).imgupload2('destroy');
				});
				
				if(img_extra == 'jcrop'){
					image_holder.find('.img:first').on('afterShow',function(){alert('shown');});
					image_holder.find('.img:first').on('hidden',function(){alert('hidden');});
					image_crop = image_holder.find('.img:first').imgAreaSelect({aspectRatio: '150:47', handles:true, onSelectEnd: function(img, selection){
							$('form#'+id+'-img-upload').find('#imgareaselect-x1').val(selection.x1);
							$('form#'+id+'-img-upload').find('#imgareaselect-y1').val(selection.y1);
							$('form#'+id+'-img-upload').find('#imgareaselect-x2').val(selection.x2);
							$('form#'+id+'-img-upload').find('#imgareaselect-y2').val(selection.y2);
							$('form#'+id+'-img-upload').find('#imgareaselect-selectedw').val(selection.width);
							$('form#'+id+'-img-upload').find('#imgareaselect-selectedh').val(selection.height);
							$('form#'+id+'-img-upload').find('#imgareaselect-resizedw').val(img.width);
							$('form#'+id+'-img-upload').find('#imgareaselect-resizedh').val(img.height);
						}
					});
					if($('form#'+id+'-img-upload').find('#imgareaselect-x1').length == 0){
					$('form#'+id+'-img-upload').append('<input type="hidden" name="imgareaselect[x1]" id="imgareaselect-x1">'+
						'<input type="hidden" name="imgareaselect[y1]" id="imgareaselect-y1">'+
						'<input type="hidden" name="imgareaselect[x2]" id="imgareaselect-x2">'+
						'<input type="hidden" name="imgareaselect[y2]" id="imgareaselect-y2">'+
						'<input type="hidden" name="imgareaselect[selectedw]" id="imgareaselect-selectedw">'+
						'<input type="hidden" name="imgareaselect[selectedh]" id="imgareaselect-selectedh">'+
						'<input type="hidden" name="imgareaselect[resizedw]" id="imgareaselect-resizedw">'+
						'<input type="hidden" name="imgareaselect[resizedh]" id="imgareaselect-resizedh">');
					}
				}
                
                image_holder.css('display','inline-block');
				
				$('#'+id+'-img-input').unbind();
				$('#'+id+'-img-input').change(function() {
					// image_holder.find('.img.colored').parent().css('width', '0%');
					image_holder.find('.upload_progress').css('width', '0%');
					image_holder.find('.upload_progress').html('&nbsp;&nbsp;0%&nbsp;&nbsp;');
					image_holder.find('.img.colored').parent().hide();
					
					// comment next line for grayscale loader
					// image_holder.find('#'+id+'-div-rel-container').hide();
					image_holder.find('.progress_wrapper').show();
					
					if($('#'+id+'-img-input').val()!=''){
						prev_img = image_holder.find('.img');
						//image_holder.find('.img').remove();
						//image_holder.append('<img class="imgupload-loader" src="img/ajax-loader-48.gif" style="display:block"/>');
						
						if(img_src == '' && (img_extra == '' || img_extra === undefined)){
							image_holder.find('.img').css('border', border_style+' !important');
							image_holder.find('.img').css('padding', '0px');
							
							image_holder.find('div').unbind();
						}
						
						if (this.files && this.files[0]) {
							var reader = new FileReader();
							//2457600 300kb
							// if(this.files[0].size > 6144000){//1Mb
								// alert('Selected file exceeded file size limit: 750kB..');
							// }else
							if(!this.value.match(/\.(jpg|jpeg|png|gif|bmp)$/i)){
								alert('File type not allowed. Upload image files only.');
							}else{
								// $('#'+id+'-remove-btn').click();
								reader.onload = function (e) {
									image_holder.find('.img').attr('src', e.target.result);
									
									image_holder.find('.img').unbind();
									image_holder.find('.img').mouseover(function() {
										image_holder.find('.img').css('border', border_hover);
									});
									image_holder.find('.img').mouseout(function() {
										image_holder.find('.img').css('border', border_style);
									});
									if(image_holder.find('.img').attr('src') != ''){
										//if(jcrop !== undefined) jcrop.destroy();
										//image_holder.find('.img:first').Jcrop({ aspectRatio : 1500 / 470, setSelect : [0,0,150,47], bgColor: 'black', bgOpacity: 0.4 }, function(){ jcrop = this; });
									}
									if(img_extra == 'jcrop'){
										image_holder.find('.img:first').imgAreaSelect({x1:0, y1:0, x2:300, y2:94, aspectRatio: '150:47', handles:true, onSelectEnd: function(img, selection){
												$('form#'+id+'-img-upload').find('#imgareaselect-x1').val(selection.x1);
												$('form#'+id+'-img-upload').find('#imgareaselect-y1').val(selection.y1);
												$('form#'+id+'-img-upload').find('#imgareaselect-x2').val(selection.x2);
												$('form#'+id+'-img-upload').find('#imgareaselect-y2').val(selection.y2);
												$('form#'+id+'-img-upload').find('#imgareaselect-selectedw').val(selection.width);
												$('form#'+id+'-img-upload').find('#imgareaselect-selectedh').val(selection.height);
												$('form#'+id+'-img-upload').find('#imgareaselect-resizedw').val(img.width);
												$('form#'+id+'-img-upload').find('#imgareaselect-resizedh').val(img.height);
											}
										});
									}
									image_holder.find('.img').show();
									 
									setTimeout( function(){
										$('#'+id+'-img-upload').ajaxSubmit({ 
											beforeSubmit: function()
												{
													if(upload_xhr !== undefined){
														upload_xhr.abort();
														image_holder.find('.upload_progress').stop();
														image_holder.find('.upload_progress').html('&nbsp;&nbsp;0%&nbsp;&nbsp;');
														image_holder.find('.upload_progress').css('width', '0%');
													}
													return true;
												},
											uploadProgress: function(event, position, total, percentComplete)
												{
													var cont_width = image_holder.find('.progress_wrapper').css('width').replace('px', '') * 1;
													image_holder.find('.upload_progress').stop().animate(
														{
															width : percentComplete + '%'
														}, 
														{
															duration : 200,
															// easing : "linear",
															progress : function(animation, progress, remaining){
																	var progress_width = image_holder.find('.upload_progress').css('width').replace('px', '') * 1;
																	var percentage_width = (progress_width/cont_width * 100).toFixed();
																	
																	image_holder.find('.upload_progress').html('&nbsp;&nbsp;' + percentage_width + '%&nbsp;&nbsp;');
																	if(percentage_width == 100){
																		setTimeout( image_holder.find('.upload_progress').html('&nbsp;&nbsp;Rendering...&nbsp;&nbsp;'), 500 );
																	}
																}
														}
													);
												},
											success:  function(responseText, statusText, xhr, $form)
												{
													var jdata = $.parseJSON(responseText);
													if(statusText == 'success'){
                                                        if ( jdata.success == 'multi' ) {
                                                            $('#'+id).val(jdata.imgs.join());
                                                        } else {
                                                            $('#'+id).val(jdata.file_name);
                                                        }
													}
													// console.log(responseText);
													// console.log(statusText);
													
													setTimeout(function(){
														image_holder.find('.progress_wrapper').hide();
														image_holder.find('.img.colored').parent().show();
														image_holder.find('.img.colored').parent().css('width', '100%');
													}, 500);
													upload_xhr = undefined;
												},
											error : function(xhr, status, errorThrown){
													image_holder.find('.upload_progress').html('&nbsp;&nbsp;Error Uploading File...&nbsp;&nbsp;');
													upload_xhr = undefined;
												},
											beforeSend : function(jqXHR, settings){
													upload_xhr = jqXHR;
													return true;
												},
											resetForm: true,
											data: opts.extra_data
										});  	
										$('#'+id+'-remove-btn').show();
									}, 500);
								}
								reader.readAsDataURL(this.files[0]);
							}
						}
					}
				});
			},
		refresh : function ()
			{
				var id = this.attr('id');
				var image_holder = this.parent();
				var file_path = this.data('file-path');
			
				if($('#'+id).val() != ''){
					image_holder.find('.img').attr('src', file_path+''+$('#'+id).val());
					$('#'+id+'-remove-btn').show();
					image_holder.find('.img.colored').parent().css('width', '100%');
					image_holder.find('.img.colored').parent().show();
					image_holder.find('.img.colored').show();
				}else{
					image_holder.find('.img').attr('src', '');
					$('#'+id+'-remove-btn').hide();
					image_holder.find('.img.colored').parent().css('width', '0%');
					image_holder.find('.img.colored').hide();
				}
			},
		clear : function ()
			{
				var id = this.attr('id');
				var image_holder = this.parent();
				
				this.val('');
				image_holder.find('.img').attr('src', '');
				$('#'+id+'-remove-btn').hide();
				image_holder.find('.img.colored').hide();
			},
		reset : function ()
			{
				var id = this.attr('id');
				var image_holder = this.parent();
				var img_src = this.data('img-empty');
			
				this.val('');
				image_holder.find('.img').attr('src', img_src);
				$('#'+id+'-remove-btn').hide();
				
			},
        destroy : function () 
            {
				var id = this.attr('id');
                var image_holder = this.parent();
                
                $('#'+id+'-img-upload').remove();
                $('#'+id+'-iframe-post-form').remove();
                $('#'+id+'').parent().remove();
            },
        select_image : function ()
            {
				var id = this.attr('id');
                
                $('#'+id+'-upload-btn').click();
            }
	};
	
	$.fn.imgupload2 = function(methodOrOptions)
	{
		if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.imgupload' );
        }    
	}
	
})(jQuery);