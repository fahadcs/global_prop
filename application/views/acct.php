<?php
include('header.php');
if($usertype== 'admin'){echo "<h1>Administration Account Controls</h1>";}
if($usertype== 'builder'){echo "<h1>Builder Account Controls</h1>";}
if($usertype== 'investor'){echo "<h1>Investor Account Controls</h1>";}
echo "<hr>";
echo "<h3>Your Account Manager</h3>";
echo "<table class='table'>";
  echo form_open(base_url()."users/update_user");
	$data_form1= array(
	  'name'=>'username',
	  'value'=>$username,
	);
	$data_form2= array(
	  'name'=>'email',
	  'value'=>$this->session->userdata('email'),
	);
	$data_form3= array(
	  'name'=>'password',
	);
  echo "<tr><td>".form_label('Change Username','username')."</td><td>".form_input($data_form1)."</td></tr>";
  echo "<tr><td>".form_label('Change Email','email')."</td><td>".form_input($data_form2)."</td></tr>";
  echo "<tr><td>".form_label('Change Password','password')."</td><td>".form_input($data_form3)."</td></tr>";
  echo "<tr><td colspan='2'>".form_submit('','Change Credentials')."</td></tr>";
echo "</table>";

if($usertype== 'admin'){
  echo "<hr>";
  echo "<h3>Builder Listing</h3>";
  echo "<table class='table'>";  
  echo "<tr><th>Name</th> <th>Email</th> </tr>"; 
  foreach($bldrs as $row){
	  echo "<tr><td><a href='".base_url()."users/superLogin/".$row['userID']."'>".$row['username']."</a></td>
	        <td>".$row['email']."</td></tr>";
  }  
  echo "</table>";
  //incoming data vars are comming from users.php controller in the account function  
  echo "<hr>";
  echo "<h3>Investor Listing</h3>";
  echo "<table class='table'>";  
  echo "<tr><th>Name</th> <th>Email</th> </tr>"; 
  foreach($invrs as $row){
	  echo "<tr><td><a href='".base_url()."users/superLogin/".$row['userID']."'>".$row['username']."</a></td>
	        <td>".$row['email']."</td></tr>";
  }  
  echo "</table>";  
}

if($usertype== 'builder'){
  echo "<hr>";
  echo "<h3>All Projects</h3>";
  echo "<table class='table'>";  
  echo "<tr><th>Title</th> <th>Status</th></tr>"; 
  foreach($projects as $row){
	  if($row['active']== 0){$stat= 'Not Approved';}else{$stat= 'Approved';}
	  echo "<tr><td><a href=''>".$row['title']."</a></td><td>".$stat."</td></tr>";
  }  
  echo "</table>"; 
}

if($usertype== 'investor'){
  echo "<hr>";
  echo "<h3>All Investments</h3>"; 
  echo "<table class='table'>";  
  echo "<tr><th>Titles</th> <th>Amounts</th> <th>Intrest</th> <th>ROI</th></tr>"; 
  foreach($pledges as $row){
	  if($row['active']== 0){$stat= 'Not Approved';}else{$stat= 'Approved';}
	  echo "<tr><td><a href='".base_url()."props/singleProp/".$row['projID']."'>".$row['title']."</a></td>
	  <td>$".$row['amt'].".00</td>
	  <td>".$row['intrest'] * 100 ."%</td>
	  <td>$".ceil($row['amt'] * $row['intrest']).".00</td></tr>";
  }  
  echo "</table>"; 
}

include('footer.php');