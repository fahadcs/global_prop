<footer class="bg-dark">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
              <div class="widget-footer-about">
                 <img src="<?php echo base_url();?>/imgs/logo-white.png" alt="">
                 <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate corporis fugiat eum autem laudantium magnam, natus, quam distinctio perspiciatis fuga molestias quasi vel illum sed ex expedita ut. Quae, eius. </p>
              </div>
          </div><!-- /.col-lg-3 -->
          <div class="col-lg-3">
            <div class="widget-footer pl-lg-4">
                <h2>Quick links</h2>
                <ul class="list-unstyled">
                  <li><a href="#">Home</a></li>
                   <li><a href="#">Projects</a></li>
                  <li><a href="#">About us</a></li>
                  <li><a href="#">Who We Work With</a></li>
                  <li><a href="#">Our Referral Program</a></li>
                </ul>
              </div>
          </div><!-- /.col-lg-3 -->
          <div class="col-lg-2">
            <div class="widget-footer">
                <h2>Help</h2>
                <ul class="list-unstyled">
                  <li><a href="#">FAQs</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Terms of Use</a></li>
                </ul>
              </div>
          </div><!-- /.col-lg-3 -->
          <div class="col-lg-3">
              <div class="widget-footer">
                <h2>Stay Connected with us</h2>
                <ul class="list-inline mb-0">
                  <li class="list-inline-item">
                    <a href="#" class="text-white"><i class="fab fa-twitter"></i></a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="text-white"><i class="fab fa-facebook-f"></i></a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="text-white"><i class="fab fa-skype"></i></a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="text-white"><i class="fab fa-linkedin-in"></i></a>
                  </li>
                </ul>
              </div>
          </div><!-- /.col-lg-3 -->
        </div><!-- /.row -->
        <div class="row copyright">
          <div class="col-lg-12">
            <p>
              © 2021 GlobalPro. All Rights Reserved
            </p>
          </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </footer><!-- /footer --> 