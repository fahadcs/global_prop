<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Project Listing</title>
<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.css"); ?>" />
<link rel="stylesheet" href="<?php echo base_url("assets/css/font-awesome.css"); ?>" />
<link rel="stylesheet" href="<?php echo base_url("assets/css/lightbox.css"); ?>" />
<script src="<?php echo base_url("assets/js/jquery-2.2.4.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/bootstrap.min.js"); ?>"></script>
<style>
	body{
		background-color:#CCC;
	}
	a:link{
		color:#999;
	}
	a:visited {
		color: #999;
	}
	a:hover {
		color: #CCC;
	}
	#wrapper{
		width:800px;
		margin:0 auto;
		margin-top:30px;
	}
	#header{
		color:#CCC;
		height:200px;
	}	
	#footer{
		margin-top:30px;
		height:50px;
		color:#FFF;		
	}
	
	.well2 {
		min-height: 20px;
		padding: 19px;
		margin-bottom: 20px;
		background-color: #301F14;
		border: 1px solid #549FB2;
		-webkit-border-radius: 6px;
		   -moz-border-radius: 6px;
				border-radius: 6px;
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
		   -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
				box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	}
	.well3 {
		min-height: 20px;
		padding: 19px;
		margin-bottom: 20px;
		background-color:#FFF;
		border: 3px solid #301F14;
		-webkit-border-radius: 6px;
		   -moz-border-radius: 6px;
				border-radius: 6px;
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
		   -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
				box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
	}

</style>
</head>
<?php 
  $userID=""; $usertype=""; $username=""; $userRef='';
  if($this->session->userdata('userID')){
	  $roiPercent= 0;
	$userID= $this->session->userdata('userID');
	$usertype= $this->session->userdata('user_type');
	$username= $this->session->userdata('username');
	$userRef=  $this->session->userdata('referralID');
    $regi_login= "Hi <a href='".base_url()."users/account/"."'>".$username."</a> || 
				 <a href='".base_url()."users/logout/'>logout</a>";
  }else{
	$regi_login= "<a href='".base_url()."users/login'>login</a> || <a href='".base_url()."users/regi'>register</a>"; 
  }
?>
<body>
	<div class="well2" id="header">
    	<img src="<?php echo base_url();?>/imgs/gp_Logo.png" width="650" height="250"/>
        <hr />
        <table style="float:left;">
          <tr>
              <td>
                <a href="<?php echo base_url(); ?>props"><img src="<?php echo base_url();?>/imgs/invest-link.png" 
                onmouseover="this.src='<?php echo base_url();?>/imgs/invest-link-hover.png'"
                onmouseout="this.src='<?php echo base_url();?>/imgs/invest-link.png'" height="20"/></a>        
              </td>
              <td>
                <a href="<?php echo base_url(); ?>props/about/"><img src="<?php echo base_url();?>/imgs/about-link.png" 
                onmouseover="this.src='<?php echo base_url();?>/imgs/about-link-hover.png'"
                onmouseout="this.src='<?php echo base_url();?>/imgs/about-link.png'" /></a>  
              </td>
          </tr>
        </table>
        
         <div style="float:right"><?php echo $regi_login; ?></div>
    </div>
	<div id="wrapper" class="well3">